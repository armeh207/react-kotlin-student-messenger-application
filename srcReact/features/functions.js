import  db from '../firebase'

const getThreadPhoto = async (user, threadParticipants, id) => {
  if (threadParticipants.length === 2) {
    const otherParticipantId = threadParticipants.find(
      (participantId) => participantId !== user.uid
    );
    const doc = await db.collection("users").doc(otherParticipantId).get();
    if (doc.exists) {
      return doc.data().photo;
    }
  } else {
    const doc = await db.collection("threads").doc(id).get();
    if (doc.exists) {
      return doc.data().threadPhoto;
    }
  }
};


export { getThreadPhoto };
