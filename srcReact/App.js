import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import './App.css';
import Messenger from './components/Messenger';
import { login, logout, selectUser } from './features/userSlice';
import { auth } from './firebase';
import Login from './components/Login';
import Settings from './components/Settings';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import Sidebar from './components/Sidebar';
import Tasks from './components/Tasks';
import './components/Sidebar.css'
function App() {
  const user = useSelector(selectUser);
  const dispatch = useDispatch();

  useEffect(() => {
    
    auth.onAuthStateChanged((authUser) => {
      if (authUser) {
        dispatch(
          login({
            uid: authUser.uid,
            photo: authUser.photoURL,
            email: authUser.email,
            displayName: authUser.displayName,
          })
        );
      } else {
        dispatch(logout());
      }
    });
  }, [dispatch]);

  return (
    <div className="App">
      <Router>
        <Routes>
          <Route path="/" element={user ? <Messenger /> : <Login />} />
          <Route path="/settings" element={<Settings />}/>
          <Route path="/tasks" element = {<Tasks/>}/>
        </Routes>
      </Router>
    </div>
  );
}

export default App;

