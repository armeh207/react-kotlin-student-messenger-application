import { Button } from '@mui/material'
import React from 'react'
import { auth, provider } from '../firebase'
import db from '../firebase'
import './Login.css'
import { useDispatch } from 'react-redux'
import { login } from '../features/userSlice'

const Login = () => {
  const dispatch = useDispatch();

  const signIn = () => {
    auth.signOut()
      .then(() => {
        auth.signInWithPopup(provider, { prompt: 'select_account' })
          .then((result) => {
            db.collection('users').doc(result.user.uid).get()
              .then((doc) => {
                if (doc.exists) {
                  // User is in "users" collection, fetch all their information
                  const user = doc.data();
                  dispatch(login(user));
                 } else {
                    // User is not in "users" collection, add them
                    const user = {
                      name: result.user.displayName,
                      email: result.user.email,
                      photo: result.user.photoURL,
                      uid: result.user.uid,
                    };
                    db.collection('users').doc(result.user.uid).set(user)
                      .then(() => {
                        dispatch(login(user));
                      })
                      .catch((error) => {
                        console.error("Error adding user to 'users' collection: ", error);
                      });
                  }
              })
              .catch((error) => {
                console.error("Error checking user in 'users' collection: ", error);
              });
          })
          .catch((error) => alert(error.message));
      })
      .catch((error) => alert(error.message));
  }

  return (
    <div className='Login'>
      <div className='login_messenger'>
        <h1>Student Messenger</h1>
      </div>
      <Button onClick={signIn}>Sign In</Button>
    </div>
  )
}

export default Login;







