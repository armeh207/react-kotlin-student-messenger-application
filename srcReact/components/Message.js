import { Avatar } from "@mui/material";
import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import { selectUser } from "../features/userSlice";
import db, { storage } from "../firebase";
import './Message.css'
import firebase from "firebase/compat/app";
const Message = ({threadId, id, data: { timestamp, email, message, senderId, fileUrl, fileType, fileName , taskName, dueDate, numFiles}}) => {
  const user = useSelector(selectUser);
  const [senderPhoto, setSenderPhoto] = useState(null);
  const handleFileDownload = async (fileName) => {
    try {
    const storageRef = storage.ref().child(`threadFiles/${threadId}/${fileName}`);
      const downloadURL = await storageRef.getDownloadURL();
      console.log('Download URL:', downloadURL);
      const link = document.createElement('a');
      link.href = downloadURL;
      link.download = fileName;
      link.target = '_blank';
      link.rel = 'noopener noreferrer';
  
      
      link.click();
    } catch (error) {
      alert("ssss")
      console.log('Error downloading attachment:', error);
      
    }
  };
  const formatTimestamp = (timestamp) => {
    if (timestamp && timestamp instanceof firebase.firestore.Timestamp) {
      return timestamp.toDate().toLocaleString();
    }
    return '';
  };
  useEffect(() => {
    if (senderId) {
      db.collection("users").doc(senderId).get().then((doc) => {
        if (doc.exists) {
          setSenderPhoto(doc.data().photo);
        }
      });
    }
  }, [senderId, fileUrl]);

  return (
    <div
      className={`message ${user.uid === senderId ? "messageSender" : "messageReceiver"}`}
    >
      <Avatar key={timestamp} src={senderPhoto} className="messagePhoto" />

      <div className="messageContents">
        {message && <p>{message}</p>}
        {fileUrl && (
          fileType.startsWith('image/') ?
          <a href={fileUrl} target="_blank" rel="noopener noreferrer">
            <img src={fileUrl} className="messageFileImage" alt="file" />
          </a>
          :
          <a onClick={() => handleFileDownload(fileName)} href={fileUrl} download >
             {fileName}
          </a>
        )}
       {taskName && (
        <div>
         <p className="task-name">Task name: {taskName}</p>
         <p className="due-date">Due date: {dueDate}</p>
         <p className="number-files">Number of files: {numFiles}</p>
        </div>
        )}
        <small>{formatTimestamp(timestamp)}</small>
      </div>
    </div>
  )
}

export default Message;




