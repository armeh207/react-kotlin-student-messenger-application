import { Avatar } from '@mui/material'
import React, { useState, useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import db from '../firebase'
import './SidebarThread.css'
import { setThread } from '../features/threadSlice'
import firebase from 'firebase/compat/app'
import { selectUser } from '../features/userSlice'
import { getThreadPhoto } from '../features/functions'

const SidebarThread = ({ id, threadName }) => {
  const dispatch = useDispatch();
  const [threadInfo, setThreadInfo] = useState([]);
  const [threadPhoto, setThreadPhoto] = useState(null);
  const [threadParticipants, setThreadParticipants] = useState([]);
  const [threadTimestamp, setThreadTimestamp] = useState(null); 
  const user = useSelector(selectUser);

  useEffect(() => {
    db.collection('threads')
      .doc(id)
      .collection('messages')
      .orderBy('timestamp', 'asc')
      .onSnapshot((snapshot) =>
        setThreadInfo(
          snapshot.docs.map((doc) => ({
            id: doc.id,
            data: doc.data(),
          }))
        )
      );
  }, [id]);

  useEffect(() => {
    db.collection('threads')
      .doc(id)
      .get()
      .then((doc) => {
        if (doc.exists) {
          setThreadParticipants(doc.data().participants);
          setThreadTimestamp(doc.data().timestamp); 
        }
      });
  }, [id]);

  const getLastMessage = () => {
    const lastMessage = threadInfo[threadInfo.length - 1]?.data;
    if (lastMessage) {
      const senderId = lastMessage.senderId;
      const message = lastMessage.message;
      const timestamp = lastMessage.timestamp && lastMessage.timestamp instanceof firebase.firestore.Timestamp
        ? lastMessage.timestamp.toDate().toLocaleString()
        : '';
      const taskName = lastMessage.taskName
      const fileType = lastMessage.fileType;
      return {
        senderId,
        message,
        timestamp,
        fileType,
        taskName
      };
    }
    if (threadTimestamp) {
      return {
        timestamp: formatTimestamp(threadTimestamp),
      };
    }
    return null;
  };
  
  const formatTimestamp = (timestamp) => {
    if (timestamp && timestamp instanceof firebase.firestore.Timestamp) {
      return timestamp.toDate().toLocaleString();
    }
    return '';
  };

  const lastMessage = getLastMessage();

  useEffect(() => {
    getThreadPhoto(user, threadParticipants, id).then((photo) =>
      setThreadPhoto(photo)
    )
  }, [user, threadParticipants, id, threadPhoto]);

  return (
    <div
      className='sidebarThread'
      onClick={() =>
        dispatch(
          setThread({
            threadId: id,
            threadName: threadName,
            participants: threadParticipants,
            threadPhoto: threadPhoto,
          })
        )
      }
    >
      <Avatar src={threadPhoto} />
      <div className='sidebarThreadDetails'>
        <h3>{threadName.length > 12 ? threadName.slice(0, 12) + "..." : threadName}</h3>
        {lastMessage && (
          <>
            {lastMessage.message ? (
              <p>{lastMessage.message.length > 37 ? `${lastMessage.message.slice(0, 37)}...` : lastMessage.message}</p>
            ) : (
              lastMessage.fileType && <p>File</p>
            )}
            {lastMessage.taskName && (
              <p>Task</p>
            )}
            <small className='sidebarThreadTime'>{lastMessage.timestamp!=null ? lastMessage.timestamp : formatTimestamp(threadTimestamp)}</small>
          </>
        )}
      </div>
    </div>
  );
};

export default SidebarThread;

