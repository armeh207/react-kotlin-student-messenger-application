import { AttachFileOutlined, MicNoneOutlined, MoreVert, SendRounded, TimerOutlined } from '@mui/icons-material'
import { Avatar, IconButton } from '@mui/material'
import React, { useEffect, useRef, useState } from 'react'
import './Thread.css'
import db, { storage } from '../firebase'
import firebase from 'firebase/compat/app'
import { useSelector } from 'react-redux'
import { selectParticipants, selectThreadId, selectThreadName } from '../features/threadSlice'
import { selectUser } from '../features/userSlice'
import Message from './Message'
import { getThreadPhoto } from '../features/functions'
import ThreadOptions from './ThreadOptions'

const Thread = () => {
  const [input, setInput] = useState('')
  const [messages, setMessages] = useState([])
  const threadName = useSelector(selectThreadName)
  const threadId = useSelector(selectThreadId)
  const user = useSelector(selectUser)
  const threadParticipants = useSelector(selectParticipants);
  const fileInputRef = useRef(null);
  const [threadPhoto, setThreadPhoto] = useState(null);
  const messagesRef = useRef(null);
  const [isPopupOpen, setIsPopupOpen] = useState(false);
  useEffect(() => {
    if (threadId) {
      const threadMessages = document.querySelector('.threadMessages');
      threadMessages.scrollTop = threadMessages.scrollHeight;
      const unsubscribeThread = db.collection('threads')
        .doc(threadId)
        .onSnapshot((snapshot) => {
          
        })
        console.log(threadId)

      const unsubscribeMessages = db.collection('threads')
        .doc(threadId)
        .collection('messages')
        .orderBy('timestamp', 'asc')
        .onSnapshot((snapshot) =>
          setMessages(
            snapshot.docs.map((doc) => ({
              id: doc.id,
              data: doc.data(),
            }))
          )
        )
        getThreadPhoto(user, threadParticipants, threadId).then((photo)=>
          setThreadPhoto(photo)
        )
        return () => {
          unsubscribeThread();
          unsubscribeMessages();
        }
    }
    
  }, [threadId, threadParticipants, user, threadPhoto])

  const sendMessage = (event) => {
    event.preventDefault();
    if (input.trim() !== '') {
      const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
  
      const messageData = {
        timestamp: serverTimestamp,
        message: input,
        senderId: user.uid,
        photo: user.photo,
        email: user.email,
      };
  
      const threadRef = db.collection('threads').doc(threadId);
  
      db.runTransaction((transaction) => {
        return transaction.get(threadRef).then((threadDoc) => {
          const threadTimestamp = threadDoc.data().timestamp;
  
          
          if (threadTimestamp < serverTimestamp) {
            transaction.update(threadRef, { timestamp: serverTimestamp });
          }
  
         
          const messagesCollection = threadRef.collection('messages');
          transaction.set(messagesCollection.doc(), messageData);
        });
      })
        .then(() => {
          setInput('');
          if (messagesRef && messagesRef.current) {
            messagesRef.current.scrollIntoView({ behavior: 'smooth' });
          }
        })
        .catch((error) => {
          console.log('Error sending message:', error);
        });
    }
  };
  
  const handleTogglePopup = () => {
    setIsPopupOpen(!isPopupOpen);
  };
  const handleKeyDown = (event) => {
    if (event.key === 'Enter') {
      sendMessage(event)
    }
  }
  const handleFileInputChange = (event) => {
    const file = event.target.files[0];
    const filePath = `threadFiles/${threadId}/${file.name}`;
    const uploadTask = storage.ref(filePath).put(file);
    const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
    uploadTask.on(
      'state_changed',
      (snapshot) => {
        
      },
      (error) => {
        
        console.log(error);
      },
      () => {
        // handle success, add file to database
        uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
          db.collection('threads')
            .doc(threadId)
            .collection('messages')
            .add({
              timestamp: serverTimestamp,
              fileUrl: downloadURL,
              senderId: user.uid,
              photo: user.photo,
              email: user.email,
              fileName: file.name,
              fileType: file.type,
            })
        });
        db.collection('threads')
          .doc(threadId)
          .update({ timestamp: serverTimestamp })
          .then(() => {
            console.log('Thread timestamp updated successfully');
          })
          .catch((error) => {
            console.log('Error updating thread timestamp:', error);
          });
      }
    );
  }

  const handleClickFileInput = () => {
    fileInputRef.current.click();
  }

  

  return (
    <div className='thread'>
      <div className='threadHeader'>
        <div className='threadHeaderContent'>
          <Avatar src={threadPhoto} />
          <div className='threadHeaderContentInfo'>
            <h4>{threadName}</h4>
          </div>
        </div>
        <IconButton onClick={handleTogglePopup}>
          <MoreVert className='threadHeaderDetails'></MoreVert>
        </IconButton>
      </div>
      {isPopupOpen && <ThreadOptions threadId={threadId} />}
      <div className='threadMessages' ref ={messagesRef}>
        {messages.map(({ id, data }) => (
          <Message threadId = {threadId} key={id} data={data}/>
        ))}
      </div>
      <div className='threadInput'>
        <form>
        <input
          type="file"
          ref={fileInputRef}
          onChange={handleFileInputChange}
          style={{ display: 'none' }}
          id= 'fileInput'
        />

        <label htmlFor="fileInput">
  <       AttachFileOutlined />
        </label>
          <input
            placeholder='Write a message...'
            type='text'
            value={input}
            onChange={(e) => setInput(e.target.value)}
            onKeyDown={handleKeyDown}
            autoComplete='new-password'
          />
          <IconButton onClick={sendMessage} disabled={!input}>
            <SendRounded />
          </IconButton>
        </form>
      </div>
    </div>
  )
}


export default Thread



