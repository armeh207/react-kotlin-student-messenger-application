import { useState, useEffect } from 'react';
import db from '../firebase';

import { auth } from '../firebase';
import { Close } from '@mui/icons-material';
import firebase from 'firebase/compat/app';
const AddGroupForm = ({ onClose }) => {
  const [users, setUsers] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [groupName, setGroupName] = useState('');
  const currentUser = auth.currentUser;

  useEffect(() => {
    const fetchUsers = async () => {
      const threadsRef = db.collection('threads');
      const snapshot = await threadsRef
        .where('participants', 'array-contains', currentUser.uid)
        .get();
  
      const userIdsSet = new Set(); 
  
      snapshot.forEach((doc) => {
        const otherParticipantId = doc.data().participants.find(
          (participantId) => participantId !== currentUser.uid
        );
        if (otherParticipantId) {
          userIdsSet.add(otherParticipantId);
        }
      });
  
      const userIds = Array.from(userIdsSet); 
  
      const userPromises = userIds.map((userId) =>
        db.collection('users').doc(userId).get()
      );
  
      const userSnapshots = await Promise.all(userPromises);
  
      const usersData = userSnapshots.map((snapshot) => snapshot.data());
  
      setUsers(usersData);
    };
  
    fetchUsers();
  }, [currentUser.uid]);
  

  const handleUserSelect = event => {
    const userId = event.target.value;

    if (!selectedUsers.includes(userId)) {
      setSelectedUsers(prevSelectedUsers => [...prevSelectedUsers, userId]);
    }
  };

  const handleUserDeselect = userId => {
    setSelectedUsers(prevSelectedUsers =>
      prevSelectedUsers.filter(id => id !== userId)
    );
  };

  const handleSubmit = async event => {
    event.preventDefault();
  
  
    if (selectedUsers.length < 2) {
      alert('Select at least 2 other participants');
      return;
    }
  
   
    if (groupName.trim() === '') {
      alert('Group name cannot be empty');
      return;
    }
  

    const sortedParticipants = [currentUser.uid, ...selectedUsers].sort();
  

    await db.collection('threads').add({
      participants: sortedParticipants,
      threadName: groupName.trim(),
      threadPhoto: '', 
      timestamp: firebase.firestore.FieldValue.serverTimestamp(),
    });
  
    alert("Group chat created successfully!")

  };
  

  return (
    <div className="overlay">
      <div className="add-group-form">
        <div className="header">
          <h2>Create Group Chat</h2>
        </div>
        <form onSubmit={handleSubmit}>
          <div className="form-control">
            <label htmlFor="group-name">Group Name:</label>
            <input
              type="text"
              id="group-name"
              value={groupName}
              onChange={event => setGroupName(event.target.value)}
            />
          </div>
          <div className="form-control">
            <label htmlFor="user-select">Add Participants:</label>
            <select id="user-select" value="" onChange={handleUserSelect}>
              <option value="" disabled>Select User</option>
              {users.map(user => (
                <option key={user.uid} value={user.uid}>
                  {user.email}
                </option>
              ))}
            </select>
            <ul className="selected-users">
              {selectedUsers.map(userId => {
                const selectedUser = users.find(user => user.uid === userId);
                return (
                  <li key={userId}>
                    {selectedUser.email}
                    <button
                      className="remove-button"
                      onClick={() => handleUserDeselect(userId)}
                    >
                      <Close />
                    </button>
                  </li>
                );
              })}
            </ul>
          </div>
          <button type="submit">Create</button>
        </form>
      </div>
    </div>
  );
};

export default AddGroupForm;
