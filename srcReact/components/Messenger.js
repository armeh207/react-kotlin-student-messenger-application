import React from 'react'
import Sidebar from './Sidebar'
import Thread from './Thread'
import './Messenger.css'
const Messenger = () => {
  return (
    <div className='messenger'>
      <Sidebar/>
      <Thread/>
    </div>
  )
}

export default Messenger
