import React, { useState, useEffect } from 'react';
import { Close, Edit, Share, GetApp } from '@mui/icons-material';
import { Button, IconButton, List, ListItem, ListItemText, ListItemSecondaryAction } from '@mui/material';
import './Tasks.css';
import db from '../firebase';
import { storage} from '../firebase';
import firebase from 'firebase/compat/app';
import { useSelector } from 'react-redux';
import { selectUser } from '../features/userSlice';
import { getThreadPhoto } from '../features/functions';

const TaskItem = ({ task, selected, onCancel, onEdit, onTaskUpdated }) => {
  const [isEditing, setIsEditing] = useState(false);
  const [editedTaskName, setEditedTaskName] = useState(task.taskName);
  const [editedTaskDescription, setEditedTaskDescription] = useState(task.taskDescription);
  const [editedDueDate, setEditedDueDate] = useState(task.dueDate);
  const [editedStatus, setEditedStatus] = useState(task.taskStatus);
  const [editedFiles, setEditedFiles] = useState([]);
  const [attachments, setAttachments] = useState([]);
  const user = useSelector(selectUser)
  const [showThreadsList, setShowThreadsList] = useState(false);
  const [threads, setThreads] = useState([]);
  useEffect(() => {
    setEditedTaskName(task.taskName);
    setEditedTaskDescription(task.taskDescription);
    setEditedDueDate(task.dueDate);
    setEditedStatus(task.taskStatus);
    setEditedFiles([]);
    fetchAttachments();
  }, [task]);

  const fetchAttachments = async () => {
    try {
      const taskFilesRef = storage.ref(`taskFiles/${task.id}`);
      const files = await taskFilesRef.listAll();
      const attachments = await Promise.all(
        files.items.map(async (item) => {
          const url = await item.getDownloadURL();
          return { name: item.name, url };
        })
      );
      setAttachments(attachments);
    } catch (error) {
      console.log('Error fetching attachments:', error);
    }
  };

  const handleEditClick = () => {
    setIsEditing(true);
    onEdit();
  };

  const handleSaveClick = async () => {
    try {
      if (editedTaskName.trim() === '' || editedTaskDescription.trim() === '' || editedDueDate.trim() === '') {
        alert('Please fill in all the fields.');
        return;
      }

      
      await db.collection('tasks').doc(task.id).update({
        taskName: editedTaskName,
        taskDescription: editedTaskDescription,
        dueDate: editedDueDate,
        taskStatus: editedStatus,
      });

      
      const taskFilesRef = storage.ref(`taskFiles/${task.id}`);
      await Promise.all(
        editedFiles.map((file) => {
          const fileRef = taskFilesRef.child(file.name);
          return fileRef.put(file);
        })
      );

     
      const updatedTask = await db.collection('tasks').doc(task.id).get();

      
      setIsEditing(false);
      onCancel();
      onEdit(updatedTask.data());
      onTaskUpdated();

      
      alert('Task details updated successfully.');
    } catch (error) {
      console.log('Error:', error);
      alert('An error occurred while updating the task details. Please try again.');
    }
  };

  const handleCancelClick = () => {
    setIsEditing(false);
    onCancel();
  };

  const handleFileChange = (e) => {
    const selectedFiles = Array.from(e.target.files);
    setEditedFiles((prevFiles) => [...prevFiles, ...selectedFiles]);
  };


  const handleRemoveAttachment = async (attachment) => {
    try {
      const taskFilesRef = storage.ref(`taskFiles/${task.id}`);
      const fileRef = taskFilesRef.child(attachment.name);
      await fileRef.delete();
      setAttachments((prevAttachments) => prevAttachments.filter((att) => att.name !== attachment.name));
      alert('Attachment removed successfully.');
    } catch (error) {
      console.log('Error removing attachment:', error);
      alert('An error occurred while removing the attachment. Please try again.');
    }
  };


 
  

  const handleDownloadAttachment = async (attachment) => {
    try {
      
  
    const storageRef = storage.ref().child(`taskFiles/${task.id}/${attachment.name}`);

      
      const downloadURL = await storageRef.getDownloadURL();
      console.log('Download URL:', downloadURL);
  
      
      const link = document.createElement('a');
      link.href = downloadURL;
      link.download = attachment.name;
      link.target = '_blank';
      link.rel = 'noopener noreferrer';
  
      
      link.click();
    } catch (error) {
      
      console.log('Error downloading attachment:', error);
      alert('An error occurred while downloading the attachment. Please try again.');
    }
  };
  
  const handleShareClick = () => {
   
    db.collection('threads')
      .where('participants', 'array-contains', user.uid)
      .get()
      .then((querySnapshot) => {
        const threads = [];
        querySnapshot.forEach((doc) => {
          threads.push({ id: doc.id, data: doc.data() });
        });
        
        setThreads(threads);
        
        setShowThreadsList(true);
      })
      .catch((error) => {
        console.log('Error fetching threads:', error);
      });
  };

  const handleThreadSelection = (thread) => {
    
    const serverTimestamp = firebase.firestore.FieldValue.serverTimestamp();
    const messageData = {
      timestamp: serverTimestamp,
      taskName: task.taskName,
      dueDate: task.dueDate,
      numFiles: attachments.length,
      senderId: user.uid
      
    };

    const threadRef = db.collection('threads').doc(thread.id);

    db.runTransaction((transaction) => {
      return transaction.get(threadRef).then((threadDoc) => {
        const threadTimestamp = threadDoc.data().timestamp;

        
        if (threadTimestamp < serverTimestamp) {
          transaction.update(threadRef, { timestamp: serverTimestamp });
        }

        
        const messagesCollection = threadRef.collection('messages');
        transaction.set(messagesCollection.doc(), messageData);
        const participantsToAdd = thread.data.participants.filter(participant => participant !== user.uid);
        const updateData = {
            sharedWith: firebase.firestore.FieldValue.arrayUnion(...participantsToAdd)
          };
        
        const taskRef = db.collection('tasks').doc(task.id);
        transaction.update(taskRef, updateData);
      });
    })
      .then(() => {
        alert('Task shared successfully');
        
        setShowThreadsList(false);
      })
      .catch((error) => {
        alert('Error sharing task:', error);
      });
  };
  
  
  
  const handleCloseButtonClick = () =>{
    setShowThreadsList(false)
  }
  
  
  
  return (

    <div className="task-item">
      {isEditing ? (
        <div className="task-item-edit">
        <label className="label">
              Task Name:
          <input className="input" type="text" value={editedTaskName} onChange={(e) => setEditedTaskName(e.target.value)} />
        </label>
        <label className="label">
              Task Description:  
          <textarea
            className="textarea"
            value={editedTaskDescription}
            onChange={(e) => setEditedTaskDescription(e.target.value)}
          />
          </label >
          <label className="label">
            Due Date
          <input
            className="input"
            type="datetime-local"
            value={editedDueDate}
            onChange={(e) => setEditedDueDate(e.target.value)}
          />
          </label>
          <div className="input-container">
            <label className="label">
              Status:
              <input
                className="checkbox"
                type="checkbox"
                checked={editedStatus === 'Done'}
                onChange={(e) => setEditedStatus(e.target.checked ? 'Done' : 'Undone')}
              />
            </label>
          </div>
          <div className="input-container">
            <label className="label">
              Attachments:
              <input className="input" type="file" onChange={handleFileChange} multiple />
            </label>
            <List>
              {attachments.map((attachment) => (
                <ListItem key={attachment.name}>
                  <ListItemText primary={attachment.name} className='attachment-name'/>
                  <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="remove" onClick={() => handleRemoveAttachment(attachment)}>
                      <Close />
                    </IconButton>
                    <IconButton edge="end" aria-label="download" onClick={() => handleDownloadAttachment(attachment)}>
                      <GetApp />
                    </IconButton>
                  </ListItemSecondaryAction>
                </ListItem>
              ))}
            </List>
          </div>
          <Button variant="contained" onClick={handleSaveClick}>
            Save
          </Button>
          <Button variant="contained" onClick={handleCancelClick}>
            Cancel
          </Button>
        </div>
      ) : (
        <div className="task-item-details">
        
          <div className="task-item-name">{task.taskName}</div>
          <div className="task-item-due-date">{task.dueDate}</div>
          <div className={`task-item-status ${task.taskStatus === 'Done' ? 'done' : ''}`}>{task.taskStatus}</div>
          {task.createdBy !== user.uid && (
            <p>Shared by: {(task.createdByEmail)}</p>
          )}
          <Edit className="button" onClick={handleEditClick} />
          {user.uid === task.createdBy && (
          <Share className="button" onClick={handleShareClick} />
        )}
          {showThreadsList && (
            <div className="threads-list">
            <Close onClick={handleCloseButtonClick}></Close>
            
            {threads.map((thread) => (
                <div key={thread.id} className="threads-list-item" onClick={() => handleThreadSelection(thread)}>
                    {thread.data.threadName}
                </div>
            ))}
            </div>
          )}
        </div>
      )}
    </div>
  );
};

export default TaskItem;





