import React, { useEffect, useState } from "react";
import db, { storage } from "../firebase";
import firebase from "firebase/compat/app";
import { FaSpinner } from "react-icons/fa";
import "./Settings.css";
function Settings() {
  const [user, setUser] = useState(null);
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploading, setUploading] = useState(false);

  const handleFileChange = (e) => {
    if (e.target.files[0]) {
      setSelectedFile(e.target.files[0]);
    }
  };

  const handleFileUpload = () => {
    if (selectedFile) {
      const file = selectedFile;
      const validTypes = ["image/jpeg", "image/png", "image/gif"];
      const validSize = 5 * 1024 * 1024; // 5MB
  
      if (!validTypes.includes(file.type)) {
        alert("Only JPEG, PNG and GIF files are allowed.");
        return;
      }
  
      if (file.size > validSize) {
        alert("File size must be less than 5MB.");
        return;
      }
  
      setUploading(true);
  
      const storageRef = storage.ref(`users/${user.uid}/profilePictures/${user.uid}-profilePicture`);
      const deleteRef = storage.ref(`users/${user.uid}/profilePictures/${user.photo.split('/')[4]}`);
  
      deleteRef
        .delete()
        .then(() => {
          console.log('Previous profile picture deleted successfully!');
        })
        .catch((error) => {
          console.error('Error deleting previous profile picture:', error);
        });
  
      const uploadTask = storageRef.put(file);
  
      uploadTask.on(
        "state_changed",
        null,
        (error) => {
          console.error("Error uploading photo:", error);
          setUploading(false);
        },
        () => {
          uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
            db.collection("users")
              .doc(user.uid)
              .update({ photo: downloadURL })
              .then(() => {
                setUploading(false);
              })
              .catch((error) => {
                console.error("Error updating photo URL:", error);
                setUploading(false);
              });
          });
        }
      );
    }
  };
  

  useEffect(() => {
    const unsubscribe = firebase.auth().onAuthStateChanged((currentUser) => {
      if (currentUser) {
        db.collection("users")
          .doc(currentUser.uid)
          .get()
          .then((doc) => {
            if (doc.exists) {
              setUser(doc.data());
            } else {
              console.log("No such document!");
            }
          })
          .catch((error) => {
            console.log("Error getting document:", error);
          });
      } else {
        setUser(null);
      }
    });

    return unsubscribe;
  }, [user]);

  return (
    <div className="settingsContainer">
      {user ? (
        <div className="settings">
          <div className="userInfo">
            <img src={user.photo} alt="User" className="avatar"></img>
            <h2>{user.name}'s Settings</h2>
            <p>Email: {user.email}</p>
            <p>UID: {user.uid}</p>
          </div>
          <div className="uploadPhoto">
            <label for="fileButton" class="fileButtonLabel">Upload your photo</label>
            <input type="file" onChange={handleFileChange} className="fileButton" accept=".jpg, .jpeg, .png"/>
            <button onClick={handleFileUpload} disabled={!selectedFile || uploading} className="fileButton">
              {uploading ? <FaSpinner className="spinner" /> : "Change Photo"}
            </button>
          </div>
        </div>
      ) : (
        <div className="loading">
          <FaSpinner className="spinner" />
        </div>
      )}
    </div>
  );
}

export default React.memo(Settings); 

