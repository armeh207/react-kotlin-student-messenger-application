import React, { useState } from 'react';
import { useSelector } from 'react-redux';
import { selectUser } from '../features/userSlice';
import { useEffect } from 'react';
import db from '../firebase';
import { storage } from '../firebase';
import './ThreadOptions.css'
import { Avatar } from '@mui/material';

const ThreadOptions = ({ threadId }) => {
  const [thread, setThread] = useState(null);
  
  const user = useSelector(selectUser);

  const handlePhotoUpload = async (event) => {
    const file = event.target.files[0];
    const storageRef = storage.ref().child(`groupChatsPhotos/${threadId}`);
    const uploadTask = storageRef.put(file);

    uploadTask.on(
      'state_changed',
      null,
      (error) => {
        console.log('Error uploading photo:', error);
      },
      () => {
        
        uploadTask.snapshot.ref.getDownloadURL().then((downloadURL) => {
          db.collection('threads')
            .doc(threadId)
            .update({ threadPhoto: downloadURL })
            .then(() => {
              console.log('Thread photo updated successfully');
              setThread({ ...thread, threadPhoto: downloadURL });
            })
            .catch((error) => {
              console.log('Error updating thread photo:', error);
              
            });
        });
      }
    );
  };

  useEffect(() => {
    const getUserById = async (userId) => {
      try {
        const userDoc = await db.collection('users').doc(userId).get();
        if (userDoc.exists) {
          return { id: userDoc.id, ...userDoc.data() };
        } else {
          return null; 
        }
      } catch (error) {
        console.log('Error getting user:', error);
        throw error;
      }
    };

    const fetchData = async () => {
      const snapshot = await db.collection('threads').doc(threadId).get();
      if (snapshot.exists) {
        const threadData = snapshot.data();
        const participants = threadData.participants;

        if (participants.length === 2) {
          const otherParticipant = participants.find(
            (participant) => participant !== user.uid
          );
          const otherParticipantUser = await getUserById(otherParticipant);

          setThread({
            id: snapshot.id,
            participants,
            otherParticipantUser,
            threadPhoto: otherParticipantUser.photo
          });
        } else {
          const otherParticipants = await Promise.all(
            participants
              .filter((participant) => participant !== user.uid)
              .map(async (participant) => {
                const participantUser = await getUserById(participant);
                return {
                  id: participant,
                  name: participantUser.name,
                  email: participantUser.email,
                  photo: participantUser.photo
                };
              })
          );

          setThread({
            id: snapshot.id,
            participants,
            otherParticipants,
            threadPhoto: threadData.threadPhoto
          });
        }
      } else {
        setThread(null);
      }
    };

    fetchData();

    return () => {
      
    };
  }, [threadId, user.uid]);

  if (!thread) {
    return null;
  }

  const { participants, otherParticipantUser, otherParticipants, threadPhoto } = thread;

  if (participants.length === 2 && otherParticipantUser) {
    return (
      <div className="popup">
        <div className="participant-info">
          <p className="participant-email">{otherParticipantUser.email}</p>
          <p className="participant-name">{otherParticipantUser.name}</p>
          <Avatar src={otherParticipantUser.photo}/>
        </div>
      </div>
    );
  } else if (participants.length === 3 && otherParticipants) {
    return (
      <div className="popup">
        <h4 className="other-participants-title">Other Participants:</h4>
        <ul className="other-participants-list">
          {otherParticipants.map((participant) => (
            <li key={participant.id} className="participant-item">
              <p className="participant-email">{participant.email}</p>
              <p className="participant-name">{participant.name}</p>
              <Avatar src={participant.photo}/>
            </li>
          ))}
        </ul>
        
         {threadPhoto!=='' ? (
            <div className="thread-photo-wrapper">
            <p>Group photo: </p>
            <img src={threadPhoto} alt="Thread" className='thread-photo'/>
            </div>
         ):(
            <p> No group photo</p>
        )}   
        <input className="photo-upload-input" type="file" onChange={handlePhotoUpload} accept=".png, .jpeg, .jpg" />
        </div>

        
      
    );
  }

  return null;
};

export default ThreadOptions;




