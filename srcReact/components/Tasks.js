import React, { useState } from 'react';
import './Tasks.css';
import db from '../firebase';
import { storage } from '../firebase';
import { selectUser } from '../features/userSlice';
import { useSelector } from 'react-redux';
import { Close} from '@mui/icons-material';
import TaskItem from './TaskItem';
import { useEffect } from 'react';
import { useCallback } from 'react';
const Tasks = () => {
  const [activeTab, setActiveTab] = useState('addTask');
  const [taskName, setTaskName] = useState('');
  const [taskDescription, setTaskDescription] = useState('');
  const [dueDate, setDueDate] = useState(null);
  const [status, setStatus] = useState(false); 
  const [files, setFiles] = useState([]);
  const user = useSelector(selectUser);
  const [userTasks, setUserTasks] = useState([]);
  const [selectedTask, setSelectedTask] = useState(null);
  const [sharedTasks, setSharedTasks] = useState([]);


  const fetchUserTasks = useCallback(async () => {
    try {
      const snapshot = await db
        .collection('tasks')
        .where('createdBy', '==', user.uid)
        .get();
  
      const tasks = snapshot.docs.map((doc) => ({
        id: doc.id,
        ...doc.data(),
      }));
  
      setUserTasks(tasks);
    } catch (error) {
      console.log('Error:', error);
    }
  }, [user.uid]);
  const fetchSharedTasks = useCallback(async () => {
    try {
      const snapshot = await db
        .collection('tasks')
        .where('sharedWith', 'array-contains', user.uid)
        .get();

      const tasks = snapshot.docs.map((doc) => {
        const taskData = doc.data();
        const sharedBy = taskData.createdBy; 
        return {
          id: doc.id,
          sharedBy,
          ...taskData,
        };
      });

      setSharedTasks(tasks);
    } catch (error) {
      console.log('Error:', error);
    }
  }, [user.uid]);
  
  useEffect(() => {
    const fetchTasks = async () => {
      fetchUserTasks();
      fetchSharedTasks();
    };
  
    fetchTasks();
  }, [fetchUserTasks, fetchSharedTasks]);
  
const handleTabClick = (tabName) => {
  setActiveTab(tabName);
};
  const handleSubmit = async (e) => {
    e.preventDefault();

    
    if (!taskName || !taskDescription) {
      alert('Please enter task name and description.');
      return;
    } else if (!dueDate) {
      alert('Please enter task due date.');
      return;
    }

    try {
      
      const taskRef = await db.collection('tasks').add({
        taskName,
        taskDescription,
        dueDate,
        taskStatus: status ? 'Done' : 'Undone', 
        createdBy: user.uid,
        sharedWith: [],
        createdByEmail: user.email
      });

      
      const taskFilesRef = storage.ref(`taskFiles/${taskRef.id}`);
      files.forEach((file) => {
        const fileRef = taskFilesRef.child(file.name);
        fileRef.put(file);
      });

      
      setTaskName('');
      setTaskDescription('');
      setDueDate(null);
      setStatus(false); 
      setFiles([]);

     fetchUserTasks();
      alert('Task added successfully.');
    } catch (error) {
      console.log('Error:', error);
      alert('An error occurred while adding the task. Please try again.');
    }
  };

  const handleTaskUpdated = () => {
    fetchUserTasks();
    fetchSharedTasks();
  };
  const handleFileChange = (e) => {
    const selectedFiles = Array.from(e.target.files);
    setFiles((prevFiles) => [...prevFiles, ...selectedFiles]);
  };

  const handleRemoveFile = (fileName) => {
    setFiles((prevFiles) => prevFiles.filter((file) => file.name !== fileName));
  };

  return (
    <div className="tasks-container">
      <div className="tabs">
        <div className={`tab ${activeTab === 'addTask' ? 'active' : ''}`} onClick={() => handleTabClick('addTask')}>
          Add Task
        </div>
        <div className={`tab ${activeTab === 'yourTasks' ? 'active' : ''}`} onClick={() => handleTabClick('yourTasks')}>
          Your Tasks
        </div>
        <div className={`tab ${activeTab === 'sharedTasks' ? 'active' : ''}`} onClick={() => handleTabClick('sharedTasks')}>
          Shared with You
        </div>
      </div>
      {activeTab === 'addTask' && (
        <form onSubmit={handleSubmit}>
          <div className="input-container">
            <label className="label">
              Task Name:
              <input className="input" type="text" value={taskName} onChange={(e) => setTaskName(e.target.value)} />
            </label>
          </div>
          <br />
          <div className="input-container">
            <label className="label">
              Task Description:
              <textarea className="textarea" value={taskDescription} onChange={(e) => setTaskDescription(e.target.value)} />
            </label>
          </div>
          <br />
          <div className="input-container">
            <label className="label">
              Due Date:
              <input className="input" type="datetime-local" value={dueDate} onChange={(e) => setDueDate(e.target.value)} />
            </label>
          </div>
          <br />
          <div className="input-container">
            <label className="label">
              Attachments:
              <input className="input" type="file" onChange={handleFileChange} multiple />
            </label>
            <div className="input-container">
            <label className="label">
              Status:
              <input className="checkbox" type="checkbox" checked={status} onChange={(e) => setStatus(e.target.checked)} />
            </label>
          </div>
          <br />
            <ul>
              {files.map((file) => (
                <li key={file.name}>
                  {file.name}
                  <Close onClick={() => handleRemoveFile(file.name)} className='remove-file-button'>
                  </Close>
                </li>
              ))}
            </ul>
          </div>
          <br />
          <button className="button" type="submit">
            Add Task
          </button>
        </form>
      )}
{activeTab === 'yourTasks' && (
  <div className="tab-content">
    {userTasks
    .sort((a, b) => {
      const dueDateA = new Date(a.dueDate);
      const dueDateB = new Date(b.dueDate);
      return dueDateA - dueDateB;
    })
    .map((task) => (
      <TaskItem
        key={task.id}
        task={task}
        selected={selectedTask === task.id}
        onEdit={() => setSelectedTask(task.id)}
        onCancel={() => setSelectedTask(null)}
        onTaskUpdated ={()=> handleTaskUpdated()}
      />
    ))}
  </div>
)}
      {activeTab === 'sharedTasks' && 
      <div className="tab-content">

        {sharedTasks
        .sort((a, b) => {
          const dueDateA = new Date(a.dueDate);
          const dueDateB = new Date(b.dueDate);
          return dueDateA - dueDateB;
        })
      .map((task) => (
        <TaskItem
          key={task.id}
          task={task}
          selected={selectedTask === task.id}
          onEdit={() => setSelectedTask(task.id)}
          onCancel={() => setSelectedTask(null)}
          onTaskUpdated={() => handleTaskUpdated()}
        />
  ))}
        </div>}
    </div>
  );
};

export default Tasks;


