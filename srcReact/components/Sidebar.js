import React, { useState, useEffect } from 'react';
import SearchIcon from '@mui/icons-material/Search';
import './Sidebar.css';
import { BorderColorOutlined, Settings, Task } from '@mui/icons-material';
import { Avatar, IconButton } from '@mui/material';
import SidebarThread from './SidebarThread';
import db, { auth } from '../firebase';
import { useSelector } from 'react-redux';
import { selectUser } from '../features/userSlice';
import firebase from 'firebase/compat/app'
import { useNavigate } from 'react-router-dom';
import { Groups, CloseOutlined } from '@mui/icons-material';
import AddGroupForm from './AddGroupForm';
const Sidebar = () => {
  const user = useSelector(selectUser);
  const [threads, setThreads] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const navigate = useNavigate();
  const [userPhotoURL, setUserPhotoURL] = useState('');
  const [isGroupsOverlayOpen, setIsGroupsOverlayOpen] = useState(false);
  const handleCloseGroupsOverlay = () => {
    setIsGroupsOverlayOpen(false);
  };
  useEffect(() => {
    const unsubscribe = db
    .collection("threads")
    .where("participants", "array-contains", user.uid)
    .orderBy("timestamp", "desc")
    .onSnapshot((snapshot) =>
      setThreads(
        snapshot.docs.map((doc) => ({
          id: doc.id,
          data: doc.data(),
        }))
      )
    );

  
    const unsubscribeAuth = auth.onAuthStateChanged((user) => {
      if (user) {
        console.log(user.uid)
        
        db.collection('users')
          .doc(user.uid)
          .get()
          .then((doc) => {
            setUserPhotoURL(doc.data().photo);
          })
          .catch((error) => {
            console.error('Error fetching user data: ', error);
          });
      }
    });  
  
    return () => {
      unsubscribe();
      unsubscribeAuth();
    };
  }, [user.uid]);
  

  const addThread = async () => {
    const receiverEmail = prompt('Enter receiver email');
    if (receiverEmail) {
      
      const receiverSnapshot = await db
        .collection('users')
        .where('email', '==', receiverEmail)
        .get();
  
      if (!receiverSnapshot.empty) {
        const receiverId = receiverSnapshot.docs[0].data().uid;
        const receiverName = receiverSnapshot.docs[0].data().name;
        const currentUser = auth.currentUser;
  
        
        const currentUserThreadsSnapshot = await db
          .collection('threads')
          .where('participants', 'array-contains', currentUser.uid)
          .get();
  
        
        const receiverThreadsSnapshot = await db
          .collection('threads')
          .where('participants', 'array-contains', receiverId)
          .get();
  
       
        const existingThread = currentUserThreadsSnapshot.docs.find(
          currentUserThread =>
            receiverThreadsSnapshot.docs.some(
              receiverThread =>
                currentUserThread.id === receiverThread.id &&
                currentUserThread.data().participants.includes(receiverId)
            )
        );
  
        if (existingThread) {
          alert('A thread with the same participants already exists');
          return;
        }
  
        
        db.collection('threads').add({
          participants: [currentUser.uid, receiverId],
          threadName: receiverName,
          timestamp: firebase.firestore.FieldValue.serverTimestamp(),
        });
      } else {
        alert(`No user found with email ${receiverEmail}`);
      }
    }
  };
  
  
  
  
  const handleSettingsClick = () => {
    
    navigate('/settings');
  };
  const handleTasksClick = () => {
    
    navigate('/tasks');
  };

  const filteredThreads = threads.filter((thread) =>
    thread.data.threadName.toLowerCase().includes(searchQuery.toLowerCase())
  );

  return (
    <>
    {isGroupsOverlayOpen && (
        <div className="groups-overlay">
        <div className="groups-overlay-content">
        <div className="groups-overlay-header">
          <h3>Create a Group Chat</h3>
          <IconButton onClick={handleCloseGroupsOverlay}>
            <CloseOutlined/>
          </IconButton>
        </div>
        <AddGroupForm/>
        </div>
      </div>
    )}
    <div className="sidebar">
      <div className="sidebarHeader">
        <div className="sidebarSearch">
          <SearchIcon className="sidebarSearchIcon" />
          <input
            placeholder="Search"
            className="sidebarInput"
            value={searchQuery}
            onChange={(e) => setSearchQuery(e.target.value)}
          />
        </div>
        <IconButton variant="outlined" id="sidebarButton">
          <BorderColorOutlined onClick={addThread} />
        </IconButton>
      </div>
      {filteredThreads.length ? (
        <div className="sidebarThreads">
          {filteredThreads.map(({ id, data: { threadName } }) => (
            <SidebarThread key={id} id={id} threadName={threadName} />
          ))}
        </div>
      ) : (
        <div className="sidebarNoThreads">
         { searchQuery.length>0 && <p className='no-threads-found'>No threads found for "{searchQuery}"</p>}
        </div>
      )}
      <div className="sidebarBottom">
        <Avatar
          className="sidebarBottomAvatar"
          src={userPhotoURL}
          onClick={() => auth.signOut()}
        />
        <IconButton onClick={handleTasksClick}>
          <Task></Task>
        </IconButton>
        <IconButton onClick={() => setIsGroupsOverlayOpen(true)}>
          <Groups/>
        </IconButton>
        
        <IconButton onClick={handleSettingsClick}>
          <Settings></Settings>
        </IconButton>
      </div>
    </div>
    </>
  );
  
};

export default Sidebar;

