import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import 'firebase/compat/storage'
const firebaseConfig = {
    apiKey: "AIzaSyDHUDyt8CEiVrkFA8CS9ECXjSAuyyQxU1M",
    authDomain: "messenger-468b4.firebaseapp.com",
    projectId: "messenger-468b4",
    storageBucket: "messenger-468b4.appspot.com",
    messagingSenderId: "765992576422",
    appId: "1:765992576422:web:fbc05a6b7f96e112ab9b67"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);
  const db = firebaseApp.firestore();
  const auth = firebase.auth();
  const provider = new firebase.auth.GoogleAuthProvider();
  const storage = firebase.storage();
  export{auth, provider, storage};
  export default db;
  
