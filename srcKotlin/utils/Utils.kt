package com.example.messengerandroid.utils
import com.example.messengerandroid.classes.Message
import com.example.messengerandroid.classes.Thread
import com.example.messengerandroid.classes.User
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.tasks.await

object Utils {
    suspend fun getThreadName(thread: Thread): String {
        val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email
        val otherUserId = getOtherUserId(thread)
        return if (thread.participants.size == 2 && otherUserId != null) {
            val user = getUserFromFirebase(otherUserId)
            user?.name ?: otherUserId
        } else {
            thread.threadName
        }
    }
     suspend fun getUserIdFromEmail(email: String?): String? {
        val usersCollection = FirebaseFirestore.getInstance().collection("users")
        val query = usersCollection.whereEqualTo("email", email).limit(1)

        val userSnapshot = query.get().await()

        return if (!userSnapshot.isEmpty) {
            val userDocument = userSnapshot.documents[0]
            userDocument.id
        } else {
            null
        }
    }
    private suspend fun getUserFromFirebase(userId: String?): User? {
        val usersCollection = FirebaseFirestore.getInstance().collection("users")

        val userDocument = usersCollection.document(userId.toString()).get().await()

        return if (userDocument.exists()) {
            val user = userDocument.toObject(User::class.java)
            user
        } else {
            null
        }
    }
    suspend fun getLastMessage(threadId: String): Message? {
        val messagesCollection = FirebaseFirestore.getInstance().collection("threads")
            .document(threadId)
            .collection("messages")

        val query = messagesCollection
            .orderBy("timestamp", Query.Direction.DESCENDING)
            .limit(1)

        val messageSnapshot = query.get().await()

        return if (!messageSnapshot.isEmpty) {
            val messageDocument = messageSnapshot.documents[0]
            messageDocument.toObject(Message::class.java)
        } else {
            null
        }
    }
    suspend fun getThreadPhoto(thread: Thread): String {
        val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email
        val otherUserId = getOtherUserId(thread)
        return if (thread.participants.size == 2 && otherUserId != null) {
            val user = getUserFromFirebase(otherUserId)
            user?.photo ?: otherUserId
        } else {
            thread.photo
        }
    }
    private suspend fun getOtherUserId(thread: Thread): String? {
        val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email

        return if (currentUserEmail != null) {
            thread.participants.firstOrNull { it != getUserIdFromEmail(currentUserEmail) }
        } else {
            null
        }
    }
    suspend fun isSenderMessage(message: Message): Boolean{
        val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email
        val userId = getUserIdFromEmail(currentUserEmail)
        return userId==message.senderId
    }
    suspend fun getSenderPhoto(message: Message): String?{
        val sender = getUserFromFirebase(message.senderId)
        return sender?.photo
    }

}