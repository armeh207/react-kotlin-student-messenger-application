package com.example.messengerandroid.classes

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId
import com.google.firebase.firestore.Exclude

data class Thread(
    @DocumentId
    val threadId: String,
    val participants: List<String>,
    val photo: String,
    val threadName: String,
    val timestamp: Timestamp,
) {

    constructor() : this("", emptyList(), "", "", Timestamp.now())

    fun getParticipantsAsString(): String {
        return participants.joinToString(", ")
    }
}