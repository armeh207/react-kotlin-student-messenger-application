package com.example.messengerandroid.classes

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId

data class Message(
    @DocumentId
    val messageId: String,
    val email: String,
    var message: String,
    val photo: String,
    val senderId: String,
    val timestamp: Timestamp
){

    constructor() : this("",  "", "", "", "", Timestamp.now())
    constructor(
        email: String,
        message: String,
        photo: String,
        senderId: String,
        timestamp: Timestamp
    ) : this("", email, message, photo, senderId, timestamp)
}
