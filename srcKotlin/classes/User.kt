package com.example.messengerandroid.classes

import com.google.firebase.Timestamp
import com.google.firebase.firestore.DocumentId

data class User(
    @DocumentId
    val id :String,
    val email: String,
    val name: String,
    val photo: String,
    val uid:String
){
    constructor() : this("", "", "", "", "")
}
