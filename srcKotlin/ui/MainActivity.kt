package com.example.messengerandroid.ui

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.messengerandroid.R
import com.example.messengerandroid.classes.Thread
import com.example.messengerandroid.adapters.ThreadAdapter
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class MainActivity : AppCompatActivity(), ThreadAdapter.OnThreadClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser!=null){
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_main)
            val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
            recyclerView.layoutManager = LinearLayoutManager(this)
            fetchThreads { threads ->
                val adapter = ThreadAdapter(threads.sortedBy { it.timestamp }.reversed(), this)
                recyclerView.adapter = adapter
            }
        }
        else{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

    }
    override fun onResume() {


        val currentUser = FirebaseAuth.getInstance().currentUser

        if (currentUser == null) {

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
        else{
            super.onResume()
            setContentView(R.layout.activity_main)
            val recyclerView: RecyclerView = findViewById(R.id.recyclerView)
            recyclerView.layoutManager = LinearLayoutManager(this)
            fetchThreads { threads ->
                val adapter = ThreadAdapter(threads.sortedBy { it.timestamp }.reversed(), this)
                recyclerView.adapter = adapter
            }
        }
    }
    override fun onThreadClick(threadId: String) {

        val intent = Intent(this, ThreadActivity::class.java)
        intent.putExtra("threadId", threadId)
        startActivity(intent)

    }

    private fun fetchThreads(callback: (List<Thread>) -> Unit) {
        val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email


        val coroutineScope = CoroutineScope(Dispatchers.Main)
        coroutineScope.launch {
            val currentUserId = currentUserEmail?.let { getUserIdByEmail(it) }


            currentUserId?.let {
                val query = FirebaseFirestore.getInstance().collection("threads")
                    .whereArrayContains("participants", it)

                val registration = query.addSnapshotListener { snapshot, exception ->
                    if (exception != null) {
                        Log.e("Error fetching threads: ", exception.toString())
                        return@addSnapshotListener
                    }

                    val threadList = mutableListOf<Thread>()


                    for (document in snapshot?.documents ?: emptyList()) {
                        val thread = document.toObject(Thread::class.java)
                        thread?.let {
                            threadList.add(it)
                        }
                    }

                    callback(threadList)
                }


            }
        }
    }


    private suspend fun getUserIdByEmail(email: String): String? {
        val usersCollection = FirebaseFirestore.getInstance().collection("users")

        val query = usersCollection.whereEqualTo("email", email)

        val userSnapshot = query.get().await()

        return if (!userSnapshot.isEmpty) {
            val userDocument = userSnapshot.documents[0]
            userDocument.id
        } else {
            null
        }
    }

}
