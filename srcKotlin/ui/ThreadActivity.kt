package com.example.messengerandroid.ui


import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.messengerandroid.R
import com.example.messengerandroid.adapters.MessageAdapter
import com.example.messengerandroid.classes.Message
import com.google.firebase.Timestamp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.firestore.Query

class ThreadActivity : AppCompatActivity() {
    private lateinit var messageAdapter: MessageAdapter
    private lateinit var progressBar: ProgressBar
    private lateinit var recyclerView: RecyclerView
    private lateinit var editText: EditText
    private lateinit var sendImageView: ImageView
    private lateinit var threadId: String
    private lateinit var messagesCollection: CollectionReference
    private lateinit var messagesListener: ListenerRegistration

    override fun onCreate(savedInstanceState: Bundle?) {
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser!=null){
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_thread)

            progressBar = findViewById(R.id.progressBar)
            recyclerView = findViewById(R.id.recyclerView)
            editText = findViewById(R.id.editText)
            sendImageView = findViewById(R.id.sendImageView)

            recyclerView.layoutManager = LinearLayoutManager(this)

            threadId = intent.getStringExtra("threadId").toString()

            messagesCollection = FirebaseFirestore.getInstance().collection("threads")
                .document(threadId)
                .collection("messages")

            sendImageView.setOnClickListener {
                val messageText = editText.text.toString().trim()
                if (messageText.isNotEmpty()) {
                    val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email.toString()
                    val senderId = FirebaseAuth.getInstance().currentUser?.uid.toString()
                    val photo = FirebaseAuth.getInstance().currentUser?.photoUrl.toString()
                    val timestamp = Timestamp.now()

                    val message = Message(currentUserEmail, messageText, photo, senderId, timestamp)

                    sendMessage(message)
                    editText.text.clear()
                }
            }
        }
        else{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }

    }
    override fun onResume() {
        super.onResume()
        val currentUser = FirebaseAuth.getInstance().currentUser
        if (currentUser == null) {

            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            finish()
        }
    }


    override fun onStart() {
        super.onStart()
        val currentUser = FirebaseAuth.getInstance().currentUser
        if(currentUser!=null){
            fetchMessages()
            startListeningForMessages()
        }
        else{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }
    }

    override fun onStop() {
        super.onStop()
        val currentUser = FirebaseAuth.getInstance().currentUser
        if(currentUser!=null){

        }
        else{
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
        }


    }

    private fun fetchMessages() {
        showProgressBar()

        messagesCollection.orderBy("timestamp", Query.Direction.ASCENDING)
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val messages = mutableListOf<Message>()
                    for (document in task.result?.documents ?: emptyList()) {
                        val message = document.toObject(Message::class.java)
                        if(message!!.message.isEmpty() ){
                            message.message = "This type of message is not supported in the application. "
                        }
                        val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email
                        message?.let {
                            messages.add(it)
                        }
                    }

                    showMessageList(messages)
                    hideProgressBar()
                } else {

                    Log.e("Error fetching messages:", task.exception.toString())
                    showMessageList(emptyList()) // Show an empty message list
                }
            }
    }

    private fun startListeningForMessages() {
        val messagesCollection = FirebaseFirestore.getInstance().collection("threads")
            .document(threadId)
            .collection("messages")

        val query = messagesCollection.orderBy("timestamp", Query.Direction.ASCENDING)

        val registration = query.addSnapshotListener { snapshot, exception ->
            if (exception != null) {
                Log.e("ThreadActivity", "Error listening for messages", exception)
                showMessageList(emptyList())
                return@addSnapshotListener
            }

            val messages = mutableListOf<Message>()
            for (document in snapshot?.documents ?: emptyList()) {
                val message = document.toObject(Message::class.java)
                val currentUserEmail = FirebaseAuth.getInstance().currentUser?.email

                message?.let {
                    messages.add(it)
                }
            }

            if (::messageAdapter.isInitialized) {
                messageAdapter.updateMessages(messages)
            } else {
                showMessageList(messages)
            }
        }

    }


    private fun stopListeningForMessages() {
        messagesListener.remove()
    }

    private fun sendMessage(message: Message) {
        messagesCollection.add(message)
            .addOnSuccessListener {
                Log.d("ThreadActivity", "Message sent successfully")
            }
            .addOnFailureListener { e ->
                Log.e("ThreadActivity", "Error sending message", e)
            }
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    private fun showMessageList(messages: List<Message>) {
        messageAdapter = MessageAdapter(messages, this)
        recyclerView.adapter = messageAdapter
    }

    private fun updateMessageList(messages: List<Message>) {
        messageAdapter.updateMessages(messages)
    }
}