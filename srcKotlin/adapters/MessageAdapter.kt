package com.example.messengerandroid.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.messengerandroid.R
import com.example.messengerandroid.classes.Message
import com.example.messengerandroid.utils.Utils.getSenderPhoto
import com.example.messengerandroid.utils.Utils.isSenderMessage
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MessageAdapter(private var messages: List<Message>, private val context: Context) :
    RecyclerView.Adapter<MessageAdapter.MessageViewHolder>() {

    private var senderMessages: List<Message> = emptyList()
    private var receiverMessages: List<Message> = emptyList()
    private val senderPhotoUrls: HashMap<String, String> = HashMap()
    private val receiverPhotoUrls: HashMap<String, String> = HashMap()

    private val coroutineScope = CoroutineScope(Dispatchers.Main)

    init {

        coroutineScope.launch {
            fetchPhotoUrls()
        }
    }

    private suspend fun fetchPhotoUrls() {
        senderMessages = messages.filter { isSenderMessage(it) }
        receiverMessages = messages - senderMessages
        for (message in senderMessages) {
            val senderId = message.senderId
            val senderPhotoUrl = getSenderPhoto(message)
            if (senderPhotoUrl != null) {
                senderPhotoUrls[senderId] = senderPhotoUrl
            }
        }

        for (message in receiverMessages) {
            val receiverId = message.senderId
            val receiverPhotoUrl = getSenderPhoto(message)
            if (receiverPhotoUrl != null) {
                receiverPhotoUrls[receiverId] = receiverPhotoUrl
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MessageViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == VIEW_TYPE_SENDER_MESSAGE) {
            val senderView = inflater.inflate(R.layout.item_message_sender, parent, false)
            SenderMessageViewHolder(senderView)
        } else {
            val receiverView = inflater.inflate(R.layout.item_message_receiver, parent, false)
            ReceiverMessageViewHolder(receiverView)
        }
    }

    override fun onBindViewHolder(holder: MessageViewHolder, position: Int) {
        val message = messages[position]
        holder.bind(message)
    }

    override fun getItemCount(): Int {
        return messages.size
    }

    override fun getItemViewType(position: Int): Int {
        val message = messages[position]
        return if (message.senderId==getStringFromSharedPreferences("USER_ID")) {
            VIEW_TYPE_SENDER_MESSAGE
        } else {
            VIEW_TYPE_RECEIVER_MESSAGE
        }
    }
    fun getStringFromSharedPreferences(key: String): String? {
        val sharedPreferences = context.getSharedPreferences("123", Context.MODE_PRIVATE)
        return sharedPreferences.getString(key, null)
    }
    fun updateMessages(newMessages: List<Message>) {
        messages = newMessages
        notifyDataSetChanged()
    }

    abstract class MessageViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        abstract fun bind(message: Message)
    }

    inner class SenderMessageViewHolder(itemView: View) : MessageViewHolder(itemView) {
        private val senderPhotoImageView: ImageView = itemView.findViewById(R.id.imageView)
        private val messageContentTextView: TextView = itemView.findViewById(R.id.messageTextView)
        private val timestampTextView: TextView = itemView.findViewById(R.id.timeTextView)

        override fun bind(message: Message) {
            val senderPhotoUrl = senderPhotoUrls[message.senderId]
            if (senderPhotoUrl != null) {
                Glide.with(itemView)
                    .load(senderPhotoUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(300, 300)
                    .placeholder(R.drawable.ic_baseline_downloading_24)
                    .into(senderPhotoImageView)
            }
            messageContentTextView.text = message.message
            timestampTextView.text = message.timestamp.toDate().toString()
        }
    }

    inner class ReceiverMessageViewHolder(itemView: View) : MessageViewHolder(itemView) {
        private val receiverPhotoImageView: ImageView = itemView.findViewById(R.id.imageView)
        private val messageContentTextView: TextView = itemView.findViewById(R.id.messageTextView)
        private val timestampTextView: TextView = itemView.findViewById(R.id.timeTextView)

        override fun bind(message: Message) {
            val receiverPhotoUrl = receiverPhotoUrls[message.senderId]
            if (receiverPhotoUrl != null) {

                Glide.with(itemView)
                    .load(receiverPhotoUrl)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .override(300, 300)
                    .placeholder(R.drawable.ic_baseline_downloading_24)
                    .into(receiverPhotoImageView)
            }
            messageContentTextView.text = message.message
            timestampTextView.text = message.timestamp.toDate().toString()
        }
    }

    companion object {
        private const val VIEW_TYPE_SENDER_MESSAGE = 1
        private const val VIEW_TYPE_RECEIVER_MESSAGE = 2
    }
}
