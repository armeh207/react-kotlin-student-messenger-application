package com.example.messengerandroid.adapters

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.messengerandroid.R
import com.example.messengerandroid.classes.Message
import com.example.messengerandroid.classes.Thread
import com.example.messengerandroid.classes.User
import com.example.messengerandroid.utils.Utils
import com.example.messengerandroid.utils.Utils.getThreadPhoto
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.tasks.await

class ThreadAdapter(private val threads: List<Thread>, private val listener: OnThreadClickListener) : RecyclerView.Adapter<ThreadAdapter.ThreadViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ThreadViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_thread, parent, false)
        return ThreadViewHolder(view)
    }

    override fun onBindViewHolder(holder: ThreadViewHolder, position: Int) {
        val thread = threads[position]
        val coroutineScope = CoroutineScope(Dispatchers.Main)
        coroutineScope.launch {
            holder.titleTextView.text = Utils.getThreadName(thread).toString()
            holder.lastMessageTextView.text = Utils.getLastMessage(thread.threadId)?.message
            val photoUrl = getThreadPhoto(thread)
            Glide.with(holder.itemView)
                .load(photoUrl)
                .into(holder.imageViewUser)
        }
        holder.timestampTextView.text = thread.timestamp.toDate().toString()

    }

    override fun getItemCount(): Int {
        return threads.size
    }

    inner class ThreadViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val titleTextView = itemView.findViewById<TextView>(R.id.userName)
        val lastMessageTextView = itemView.findViewById<TextView>(R.id.lastMessageTextView)
        val timestampTextView = itemView.findViewById<TextView>(R.id.timestampTextView)
        val imageViewUser = itemView.findViewById<ImageView>(R.id.imageViewUser)
        init {
            itemView.setOnClickListener {
                val position = adapterPosition
                if (position != RecyclerView.NO_POSITION) {
                    val thread = threads[position]
                    listener.onThreadClick(thread.threadId)
                }
            }
        }
    }
    interface OnThreadClickListener {
        fun onThreadClick(threadId: String)
    }



}
